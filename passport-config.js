const LocalStrategy = require('passport-local').Strategy;
const bcrypt = require('bcrypt')


function initialize(passport, getUserByEmail, getUserById) {
    const authenticateUser = async (email, pass, done) => {
        // ambil email sebagai user
        const user = getUserByEmail(email);   

        // jika email tidak ditemukan
        if (user == null) {
            return done(null, false, {
                message : 'no user with that email'
            })
        }

        try {
            if (await bcrypt.compare(pass, user.pass)){
                return done(null, user)
            } else {
                return done(null, false, {
                    message: 'Password incorrect'
                })
            } 
        } catch (error) {
            return done(error)
        }
    }
    passport.use(new LocalStrategy({email:'email'}, authenticateUser));
    passport.serializeUser((user, done) => {
        done(null, user.id)
    })
    passport.deserializeUser((id, done) => {
        return done(null, getUserById(id))
    })
}

module.exports = initialize