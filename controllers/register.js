const handleRegister = (req, res, db, bcrypt) => {
    const saltRounds = 10;
    const {email, pass, name} = req.body;
    if (!email || !name || !pass) {
        return res.status(400).json('incorrect form submission');
    }
    const hash = bcrypt.hashSync(pass, saltRounds);

    db.transaction(trx => {
        trx.insert({
                pass: hash,
                email: email
            })
            .into('login')
            .returning('email')
            .then(loginEmail => {
                return trx('users')
                    .returning('*')
                    .insert({
                        email: loginEmail[0],
                        name: name,
                        entries: 0,
                        joined: new Date()
                    })
                    .then(user => {
                        res.json(user[0]);
                    })
            })
            .then(trx.commit)
            .catch(trx.rollback)
    })
    .catch(err => res.status(400).json(err + '\n unable to register'))
}

module.exports = {
    handleRegister: handleRegister
};