const Clarifai = require('clarifai');

//You must add your own API key here from Clarifai.
const app = new Clarifai.App({
    apiKey: '1c5ced48c05d49d78992b04cd1697bee'
});

const handleApiCall = (req, res) => {
    app.models
        .predict(Clarifai.FACE_DETECT_MODEL, req.body.input.replace(/^data:image\/(.*);base64,/, ''))
        .then(data => {
            res.json(data);
        })
        .catch(err => res.status(400).json('unable to work with API'))
}

const handleImage = (req, res, db) => {
    const {id} = req.body;
    // need perhatian
    db('users').where('id', id)
        .increment('entries', 1)
        .returning('*')
        .then(data => {
            res.json(data[0].entries);
        }).catch(err => console.log(err))
        
        .catch(err => res.status(400).json('unable to get entries' + err))
}

module.exports = {
    handleImage,
    handleApiCall
}