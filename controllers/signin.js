const handleSignin = (db, bcrypt) => (req, res) => {
    const saltRounds = 10;
    const {email, pass} = req.body;
    if (!email || !pass) {
        return res.status(400).json('incorrect form submission');
    }
    db('login').where('email', email).select('email', 'pass')
    .then(data => {
        let passwd = data[0].pass;
        let isValid = bcrypt.compareSync(pass, passwd);
        if (isValid) {
            return db('users').where('email', email).select('*')
                .then(user => {
                    res.json(user[0]);
                }).catch(err => res.json('unable to get user'))
        } else {
            // TODO : tampilkan password salah
            res.json('email atau pass salah')
        }
    }).catch(err => res.json(err))
}

module.exports = {
  handleSignin: handleSignin
}